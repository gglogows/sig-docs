# Automotive SIG Vocabulary

This page defines terms used by the Automotive SIG.
If something is unclear, let us know.

## CentOS Stream

CentOS Stream is a continuously delivered distribution that tracks just ahead
of Red Hat Enterprise Linux (RHEL) development. CentOS Stream is positioned midstream
between Fedora and RHEL.

## OSBuild manifests

OSBuild manifests are JSON files that instruct [OSBuild](https://www.osbuild.org/)
how to build images.

## Special Interest Group (SIG)

A SIG is a group of people who are interested in collaborating on the same goal.

The CentOS Automotive SIG is a group of people who are interested in developing
an operating system (OS) based on CentOS Stream that could be used in an automotive
environment. This OS will not be safety certified, however, and should be considered
as a research project or a proof of concept (POC).
