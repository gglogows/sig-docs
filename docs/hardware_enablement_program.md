# Hardware enablement program

The hardware enablement program fosters collaboration and enables the development of intelligent and connected vehicle applications.
Its primary objective is to streamline and facilitate the process of enabling and certifying[^1] reference hardware platforms
for Red Hat In-Vehicle Operating System (OS).

As a silicon vendor, our hardware enablement program empowers you with an efficient process to seamlessly integrate your hardware platform
into the Red Hat In-Vehicle OS ecosystem through
[CentOS Stream Automotive Special Interest Group's (SIG's) Automotive Stream Distribution (AutoSD)](https://blog.centos.org/2022/03/centos-automotive-sig-announces-new-autosd-distro/).
AutoSD is based on CentOS Stream, with a few divergences, and it serves as our community presence in its function as an upstream repository
to Red Hat In-Vehicle OS, similar to how CentOS Stream operates in relation to RHEL.
Red Hat In-Vehicle OS is a functionally safe, certified, software-defined vehicle (SDV) OS currently in development by Red Hat.

The Red Hat In-Vehicle OS development team must ensure the automotive kernel maintains traceability to the
[upstream Linux mainline kernel, maintained by Linus Torvalds,](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/)
to effectively support the long life cycles necessary for the automotive industry and to benefit from the stabilizing work
done in RHEL and AutoSD as immediate upstreams to Red Hat In-Vehicle OS.

_Red Hat In-Vehicle OS Upstream and downstream code flow_
![Workflow Diagram](img/HWEnablementFlow.png)

To cater to different hardware partner prototyping and development needs, Red Hat has two distinct tracks.

* **Experimental**
The experimental track is ideal if you are a silicon vendor who prefers to embark on the process independently because it provides
you with the opportunity to test the compatibility of your hardware with AutoSD. This initial testing phase allows you to gain
valuable insights and make adjustments.

* **Production**
The production track offers a comprehensive solution if you are a silicon vendor who is ready to release your hardware platforms,
supported by Red Hat In-Vehicle OS, to the market. This track encompasses enablement, integration, and a rigorous hardware certification process
that ensures the reference platform meets the stringent stability, reliability, and security requirements that the automotive industry expects from you,
as an automotive hardware vendor, and Red Hat.
After certification, the hardware platform joins the official list of supported devices, thereby becoming available to Red Hat automotive customers.

_Red Hat In-Vehicle OS Experimental and Production tracks of the hardware enablement workflow_
![Diagram that depicts the experimental and production tracks of the hardware enablement workflow.](img/content-flow-diagram.png)
In above diagram it's shown the experimental and production tracks of the hardware enablement workflow.
The experimental track allows the hardware suppliers to work mostly independently to enable hardware
to work with Linux mainline, Fedora Always Ready Kernel (or ARK), or AutoSD.

As a hardware vendor, you can work on one or both tracks simultaneously.
For instance, you might opt to begin with the experimental track to swiftly validate a proof of concept while
concurrently working to upstream your drivers and initiate progress on the production track. This parallel approach
allows you to explore different avenues and tailor your journey to best suit your unique requirements.
Even within the experimental track, Red Hat recommends that you upstream your drivers into Linux mainline to speed up production enablement.

When you and Red Hat agree to move forward with the production track, Red Hat expects that the drivers are already
accepted [upstream](https://www.redhat.com/en/blog/what-open-source-upstream) into the Linux mainline kernel,
so Red Hat can backport them in the next possible Red Hat In-Vehicle OS `kernel-automotive` release.

Red Hat provides you with a hardware test suite to run during the production track that consists of procedures to certify your hardware on Red Hat software.
The test suite is accompanied by a guide with information about the entire certification process, test methodology, results evaluation,
and instructions for how to set up the certification environment, test the systems or components you intend to certify,
and submit the results to Red Hat for verification.

After Red Hat reviews and approves these test results, Red Hat lists your hardware in the [Red Hat Ecosystem Catalog](https://catalog.redhat.com/hardware).

## Advantages of upstreaming your drivers

By upstreaming your hardware drivers and embracing an open source approach, you can reap numerous tangible advantages while
contributing to a vibrant ecosystem, leveraging the collective knowledge and expertise of our community while also fueling
your innovation and continuous improvement.
Engaging with open source technologies not only encourages collaboration and knowledge sharing but also enhances the quality
and reliability of the solutions we create together.

Increased Visibility and Market Adoption:: Mainline inclusion boosts mindshare and hardware compatibility,
leading to greater market adoption and brand recognition.

* **Improved Compatibility**
Mainline inclusion ensures broader Linux distribution support and provides hardware compatibility out-of-the-box.
Whereas out-of-tree drivers continuously need to maintain compatibility with the baseline.

* **Reduced Maintenance Burden**
Linux community support means a shared responsibility for driver maintenance, which allows silicon vendors like you to maintain fewer versions
of the OS while benefiting from broader testing, so you can focus resources on other critical areas.

* **Collaboration and Feedback**
Community collaboration provides valuable feedback, leading to higher quality drivers improved performance, and enhanced reliability.

## Ideal Red Hat In-Vehicle OS hardware

In the spirit of collaboration, these criteria are aspirations during the experimental track. However, as we progress towards the production track,
these items will be treated more definitively as requirements. We will discuss exceptions on a case-by-case basis.

1. Hardware drivers must meet the standards set by Red Hat in the upstream relevant to the Red Hat In-Vehicle OS stack.
   By aligning with Red Hat's acceptable driver criteria, hardware suppliers can ensure seamless integration and compatibility with our software ecosystem.
2. We encourage hardware suppliers to collaborate with current Red Hat build environment technology, leveraging tools such as RPM [Red Hat Package Manager](https://rpm.org/)
   and [OSTree](https://ostreedev.github.io/ostree/).
   This compatibility facilitates a smooth and efficient integration process by reducing complexities and enhancing interoperability.
3. To enable advanced virtualization capabilities, the hardware platform must provide EL2 privileges for aarch64 architecture
   or "ring-1" for the x86 architecture.
   This allows the implementation of KVM and enables you to launch virtual machines, thus expanding the possibilities
   for intelligent and connected vehicle applications.
4. It is advantageous for the hardware to include a capable GPU that has been upstreamed.
   This ensures optimal performance and support for graphics-intensive applications, contributing to a richer user experience
   and opening doors to a broader range of automotive use cases.
5. The hardware (specifically aarch64 architecture) must support one of the standard ARM
   [SystemReady](https://www.arm.com/architecture/system-architectures/systemready-certification-program) profiles.
   For example, compliance with profiles like SystemReady-IR or SystemReady-ES would enhance the system's reliability,
   robustness, and compatibility with industry standards.

## Out-of-tree driver modules

Red Hat builds, ships, and supports only code that has been accepted into a wide community in a recognized open source repository.

!!! note

     Red Hat In-Vehicle OS in this context means the general availability (GA) product release, not artifacts based on experimental downstream releases.

Red Hat does not oppose the creation of drivers that hardware partners decide to maintain out-of-tree.
However, the Red Hat In-Vehicle OS does not include out-of-tree drivers in distributions or sample images.
The Red Hat In-Vehicle OS does not include proprietary code or code that cannot be accepted upstream to enable out-of-tree drivers.
To work with Red Hat In-Vehicle OS, the out-of-tree driver must be able to compile against a Red Hat In-Vehicle OS binary build.
In other words, the the out-of-tree driver must work without kernel recompilation.

**Additional resources**

* [Kernel Virtual Machine (KVM) open source project](https://linux-kvm.org/page/Main_Page)
* [Virtual machine components and their interaction in RHEL](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/introducing-virtualization-in-rhel_configuring-and-managing-virtualization#rhel-virtual-machine-components-and-their-interaction_introducing-virtualization-in-rhel)
* [Driver Implementer's API Guide](https://www.kernel.org/doc/html/latest/driver-api/index.html)
* [Building External (out-of-tree) Modules](https://docs.kernel.org/kbuild/modules.html)
