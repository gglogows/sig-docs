# Resizing the pi's partition

Depending on how you have flashed your system on your SD card, it is likely
that the system did not extend to the full size available on the SD card.

In other words, you may only be using, for example 8G of a 32G SD card (or worse,
8G of a 128G SD card).

Here are some steps you can follow to resize the `/` partition.

!!! note
    All the commands here need to be run as `root`, or via `sudo`.

!!! warning
    It is not recommended to resize a running partition, we recommend that the
    SD you want to resize be mounted via USB or a SD card reader to a running
    system. Similarly, if the partitions have been automatically mounted by your
    operating system, do un-mount them before resizing them (for example using:
    `umount /dev/sda3`).

Finally, this documentation uses CLI tools to resize the partition, depending on
the system you run, there are a few graphical tools (e.g.: `gnome-disks`, `gparted`)
which may also be convenient to use.

## Find out the partition to resize and its available size

You can use `lsblk -l` to list all the block devices

This is an example output:

```console
#  lsblk -l
NAME      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda         8:0    1 29.7G  0 disk
sda1        8:1    1  100M  0 part
sda2        8:2    1  300M  0 part
sda3        8:3    1  7.6G  0 part
mmcblk1   179:0    0 29.7G  0 disk
mmcblk1p1 179:1    0  100M  0 part /boot/efi
mmcblk1p2 179:2    0  200M  0 part /boot
mmcblk1p3 179:3    0 29.4G  0 part /
```

In this output we have two disks:

* `sda` which is the 32G SD card mounted via USB
* `mmcblk1` which is the 32G SD card running the current sytem (which you can see via the mountpoints)

So, the disk we want to resize is `sda`, more precisely, we want to extend the
`sda3` partition which is currently 7.6G.

## Resize the partition

We will use the tool `parted` to do this.

If you are running a CentOS Stream or Fedora system, you can install it using:

```console
dnf install parted
```

Once, installed you can run it as follow:

```console
parted /dev/sda
```

You can then ask `parted` to list all the partition using the command: `print`.

Finally, you can resize the partition using the command: `resizepart <partition_number>`.

The output should look something like this:

```console
# parted /dev/sda
GNU Parted 3.4
Using /dev/sda
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) print
Model: Mass Storage Device (scsi)
Disk /dev/sda: 31.9GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  106MB   105MB   primary  fat16        esp
 2      106MB   420MB   315MB   primary  ext4
 3      420MB   8590MB  8169MB  primary  ext4

(parted) resizepart 3
End?  [8590MB]? 29.5G
(parted)
```

In this output, you can see we have resized the partition `3` to `29.5G`.

## Check the file system

Once resizing a partition, it is a good idea to check the resulting file system.
You can do this using `e2fsck`.

Here is an example output:

```console
# e2fsck -f /dev/sda3
e2fsck 1.46.5 (30-Dec-2021)
Pass 1: Checking inodes, blocks, and sizes
Pass 2: Checking directory structure
Pass 3: Checking directory connectivity
Pass 4: Checking reference counts
Pass 5: Checking group summary information
root: 45737/498736 files (0.1% non-contiguous), 675741/1994496 blocks
```

## Resize the file system

Once the partition has been extended and the file system checked, we still need
to resize the file system itself. This is done using `resize2fs`.

Here is an example output:

```console
# resize2fs /dev/sda3
resize2fs 1.46.5 (30-Dec-2021)
Resizing the filesystem on /dev/sda3 to 5024297 (4k) blocks.
The filesystem on /dev/sda3 is now 5024297 (4k) blocks long.
```

## Last words

At this point, your SD card should be all ready to be used. You can turn off your
system, place that SD card you've just worked on into the pi and boot it!

You can check that the partition has indeed been increased using `df -h` once
the system booted.
